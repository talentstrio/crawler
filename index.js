var fs = require("fs");
var request = require("request");
var cheerio = require("cheerio");
var iconv = require("iconv-lite");
var async = require("async");
//var waterfall = require('async-waterfall');
//var data = require("./data");

var resource_file = './resource.json';
var resource;
var url = [];
var data_num = 0;
var result = [];

/*async.waterfall([
  function(callback){
		fs.readFile(resource_file, 'utf8', function (err, data) {
			if (err) throw err;
			resource = JSON.parse(data);
			console.log(resource.length);
			callback(null, resource);
		});
  },
  function(arg1, callback){
		console.log(arg1);
		for (i=0; i<resource.length; i++) {
			for (j=0; j<resource[i].sql.length; j++) {
				for (k=0; k<resource[i].sql[j].page_value; k++) {
					url[data_num] = resource[i].url + resource[i].area_title + '=' + resource[i].sql[j].area_value + '&' + resource[i].page_title + '=' + k;
					data_num++;
				}
			}
		}
		callback(null, url);
  },
  function(url, callback){
    // arg1 now equals 'three'
		console.log(url);
		for (i=0; i<url.length; i++) {
			async.waterfall([
				function (callback) {
					var html, $, titles;
					request({
						url: url[i],
						method: "GET",
						encoding: null
					}, function (err, res, body) {
						if (err || !body) return;
						html = iconv.decode(body, 'big5');
						$ = cheerio.load(html);

						$(".link04").contents().filter(function (){
							return this.nodeType == 3;
						}).wrap('<div />');

						titles = $("td.link04 div");
						console.log(titles.length);
						callback(null, titles, $);
					});

				},
				function (titles, $, callback) {
					for (var i=0; i<titles.length; i++) {
						result.push($(titles[i]).text());
					}

					result = result.filter(v=>v!='\n');
					result = result.filter(v=>v!=' ');
					for (var i=0; i<result.length; i++) {
						result[i] = result[i].replace(/(\r\n|\n|\r|\s)/gm, "");
					}
					callback(null, result);
				}
			], function (err, result) {
			  // result now equals 'done'
				console.log(result);
				fs.writeFileSync("result.json", JSON.stringify(result));
			});
		}
    callback(null, "done");
  }
], function (err, result) {
  // result now equals 'done'
	console.log(result);
});*/

request({
	url: "http://www.5151.tw/tmed/city2.php?areaid2=400&page=1",
	method: "GET",
	encoding: null
}, function (err, res, body) {
	if (err || !body) return;
		var html = iconv.decode(body, 'big5');
		var $ = cheerio.load(html);

		$("tr").contents("td").filter(function (){
			return this.nodeType == 3;
		}).wrap('<div class="ttt" />');

		var titles = $("div.ttt");

		for (var i=0; i<titles.length; i++) {
			result.push($(titles[i]).text());
		}

		for (var x in result) {
			console.log(result[x]);
		}

		//result = result.filter(v=>v!='\n');
		//result = result.filter(v=>v!=' ');
		for (var i=0; i<result.length; i++) {
			result[i] = result[i].replace(/(\r\n|\n|\r|\s)/gm, "");
		}

		fs.writeFileSync("result.json", JSON.stringify(result));
})

/*async.waterfall([
	// read resource.json
	function (cb) {
		fs.readFile(resource_file, 'utf8', function (err, data) {
			if (err) throw err;
			resource = JSON.parse(data);
			console.log(resource.length);
			cb(null, resource);
		});
	},

	function (resource, cb) {
		for (i=0; i<resource.length; i++) {
			for (j=0; j<resource[i].sql.length; j++) {
				for (k=1; k<=resource[i].sql[j].page_value; k++) {
					url[data_num] = resource[i].url + resource[i].area_title + '=' + resource[i].sql[j].area_value + '&' + resource[i].page_title + '=' + k;
					data_num++;
				}
			}
		}
		cb(null, url);
	},

	function (url, cb) {
		for (var i in url) {
			console.log(url[i]);
			request({
				url: url[i],
				method: "GET",
				encoding: null
			}, function (err, res, body) {
				if (err || !body) return;
				var html = iconv.decode(body, 'big5');
				var $ = cheerio.load(html);

				$(".link04").contents().filter(function (){
					return this.nodeType == 3;
				}).wrap('<div />');

				var titles = $("td.link04 div");

				for (var i=0; i<titles.length; i++) {
					result.push($(titles[i]).text());
				}

				result = result.filter(v=>v!='\n');
				result = result.filter(v=>v!=' ');
				for (var i=0; i<result.length; i++) {
					result[i] = result[i].replace(/(\r\n|\n|\r|\s)/gm, "");
				}
				console.log(url[i] + result);
				fs.writeFileSync("result.json", JSON.stringify(result));
			});
		}
		cb(null, 'done');
	}
], function (err, result) {
  // result now equals 'done'
	console.log(result);
});*/
